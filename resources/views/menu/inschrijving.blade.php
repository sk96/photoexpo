@extends('layouts.htmltop')
@section('content')
    <body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="logo">
                    <img src="img/dagfoto2.png"/>
                </div>
                <div class="titel">
                    <h1>Inschrijvingen</h1>
                </div>
                <div class="content">
                    <h2>Voorwaarden</h2>
                    <ul>
                        <p>
                        <li><i class="fas fa-camera-retro"></i> Inschrijven voor de workshops en demo's is verplicht.
                        </li>
                        </p>
                        <p>
                        <li><i class="fas fa-camera-retro"></i> Om sommige workshops en demo's te kunnen aanbieden,
                            vragen we een vergoeding van 10 euro.<br> Je inschrijving is pas definitief na betaling.
                        </li>
                        </p>
                        <p>
                        <li><i class="fas fa-camera-retro"></i> Indien we dit bedrag niet tijdig ontvangen, wordt je
                            inschrijving geannuleerd.
                        </li>
                        </p>
                        <p>
                        <li><i class="fas fa-camera-retro"></i> We vragen je om ten laatste 10 minuten voor de start van
                            de workshop/demo aanwezig te zijn.<br> Als de workshop gestart is, gaan de deuren dicht.
                        </li>
                        </p>
                    </ul>
                    <h2>Opgelet</h2>
                    <p><b>De inschrijvingsperiode voor cursisten loopt van 21 tot en met 27 mei.</b></p>
                    <p>Elke cursist kan zich dan voor maximum één workshop of demo inschrijven.<br>
                        Hierna worden de inschrijvingen opengesteld, waardoor je ook als niet-cursist kan
                        inschrijven<br> of je als cursist kan inschrijven voor meerdere workshops.</p>
                    <p>Zondagavond 10 juni om 20:00 worden de inschrijvingen afgesloten!</p>

                    <h2>Smaak te pakken?</h2>
                    <p>Heb je de smaak te pakken en wil je je inschrijven voor een workshop of demo?</p>
                    <p>Wees er dan snel bij! Want de plaatsen zijn beperkt!</p>
                    <p>Via onderstaande knop kan je naar onze plannings pagina gaan.<br>
                        Hier kan je klikken op een demo of workshop naar keuze en je inschrijven.
                    </p>
                    <div class="btntest">
                        <p><a class="knop" href="{{ route('show.event') }}"><i class="fas fa-arrow-left"></i> Naar de
                                planning!</a></p>
                    </div>
                    <div class="btntest">
                        <p><a class="knop" href="{{ route('home') }}"><i class="fas fa-arrow-left"></i> Terug naar
                                menu</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src={{ URL::asset('js/jquery.min.js')}} ></script>
    <script type="text/javascript" src={{ URL::asset('js/jquery-1.11.1.min.js')}} ></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.textillate.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.lettering.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.fittext.js') }}"></script>
    <script type="text/javascript">
        $('h1').textillate({
            in: {effect: 'fadeInRightBig', delay: 200},
        });
    </script>
    </body>
    </html>
@endsection