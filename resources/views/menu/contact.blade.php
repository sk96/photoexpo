@extends('layouts.htmltop')
@section('content')
    <body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="logo">
                    <img src="img/dagfoto.png"/>
                </div>
                <div class="titel">
                    <h1>Contact</h1>
                </div>
                <form class="formulier">
                    <p>
                        <label for="voornaam">Voornaam:</label>
                        <br>
                        <input type="text" id="voornaam"/>
                        <span class="error" id="voornaam_error"></span>
                    </p>
                    <p>
                        <label for="achternaam">Achternaam:</label>
                        <br>
                        <input type="text" id="achternaam"/>
                        <span class="error" id="achternaam_error"> </span>
                    </p>
                    <p>
                        <label for="email">Email:</label>
                        <br>
                        <input type="text" id="email"/>
                        <span class="error" id="email_error"></span>
                    </p>
                    <p>
                        <label for="telefoonnummer">Telefoonnummer:</label>
                        <br>
                        <input type="text" id="telefoonnummer"/>
                        <span class="error" id="telefoonnummer_error"></span>
                    </p>
                    <p>
                        <label>Geef hier uw boodschap in a.u.b?</label>
                        <br>
                        <textarea id="mijnTekst"></textarea>
                    </p>
                    <p>
                        <input type="button" value="Verstuur" id="knop_verstuur" onclick="verstuur()">
                    </p>
                </form>
                <div class="direction">
                    <h2>Waar</h2>
                    <p>
                        De Dag van de Fotografie gaat door op de mooie mijnsite van Heusden-Zolder.
                    </p>
                    <p>
                        CVO De Verdieping en ZLDR Luchtfabriek
                        <br> Schachtplein 1,
                        <br>3550 Heusden-Zolder
                    </p>
                    <div class="map-responsive">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2508.7223089188083!2d5.3261543159791085!3d51.03974935260793!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c1259529abc86f%3A0x63a362a398e74fb2!2sZLDR+AIR+FACTORY!5e0!3m2!1sen!2sbe!4v1520425249684"
                                width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                    <!-- Begin MailChimp Signup Form -->
                    <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
                    <style type="text/css">
                        #mc_embed_signup {
                            background: #fff;
                            clear: left;
                            font: 14px Helvetica, Arial, sans-serif;
                        }

                        /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
                           We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
                    </style>
                    <div id="mc_embed_signup">
                        <form action="https://kuyperssven.us17.list-manage.com/subscribe/post?u=324af41d7ea3cced73ebfa9ac&amp;id=1cd9ac4778"
                              method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form"
                              class="validate" target="_blank" novalidate>
                            <div id="mc_embed_signup_scroll">
                                <h2>Registreer je op onze nieuwsbrief</h2>
                                <h6>Indien u zich inschrijft voor onze nieuwsbrief, gaat u akkoord tot het bewaren van
                                    uw gegevens.</h6>
                                <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
                                <div class="mc-field-group">
                                    <label for="mce-EMAIL">Email Address <span class="asterisk">*</span>
                                    </label>
                                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                                </div>
                                <div class="mc-field-group">
                                    <label for="mce-FNAME">Voornaam </label>
                                    <input type="text" value="" name="FNAME" class="" id="mce-FNAME">
                                </div>
                                <div class="mc-field-group">
                                    <label for="mce-LNAME">Achternaam </label>
                                    <input type="text" value="" name="LNAME" class="" id="mce-LNAME">
                                </div>
                                <div id="mce-responses" class="clear">
                                    <div class="response" id="mce-error-response" style="display:none"></div>
                                    <div class="response" id="mce-success-response" style="display:none"></div>
                                </div>
                                <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text"
                                                                                                          name="b_324af41d7ea3cced73ebfa9ac_1cd9ac4778"
                                                                                                          tabindex="-1"
                                                                                                          value="">
                                </div>
                                <div class="clear"><input type="submit" value="Subscribe" name="subscribe"
                                                          id="mc-embedded-subscribe" class="button"></div>
                            </div>
                        </form>
                        <a href="https://kuyperssven.us17.list-manage.com/unsubscribe?u=324af41d7ea3cced73ebfa9ac&id=1cd9ac4778">
                            Klik hier om u uit te schrijven op onze nieuwsbrief</a>
                    </div>
                    <script type='text/javascript'
                            src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
                    <script type='text/javascript'>(function ($) {
                            window.fnames = new Array();
                            window.ftypes = new Array();
                            fnames[0] = 'EMAIL';
                            ftypes[0] = 'email';
                            fnames[1] = 'FNAME';
                            ftypes[1] = 'text';
                            fnames[2] = 'LNAME';
                            ftypes[2] = 'text';
                            fnames[3] = 'ADDRESS';
                            ftypes[3] = 'address';
                            fnames[4] = 'PHONE';
                            ftypes[4] = 'phone';
                            /*
                            * Translated default messages for the $ validation plugin.
                            * Locale: NL
                            */
                            $.extend($.validator.messages, {
                                required: "Dit is een verplicht veld.",
                                remote: "Controleer dit veld.",
                                email: "Vul hier een geldig e-mailadres in.",
                                url: "Vul hier een geldige URL in.",
                                date: "Vul hier een geldige datum in.",
                                dateISO: "Vul hier een geldige datum in (ISO-formaat).",
                                number: "Vul hier een geldig getal in.",
                                digits: "Vul hier alleen getallen in.",
                                creditcard: "Vul hier een geldig creditcardnummer in.",
                                equalTo: "Vul hier dezelfde waarde in.",
                                accept: "Vul hier een waarde in met een geldige extensie.",
                                maxlength: $.validator.format("Vul hier maximaal {0} tekens in."),
                                minlength: $.validator.format("Vul hier minimaal {0} tekens in."),
                                rangelength: $.validator.format("Vul hier een waarde in van minimaal {0} en maximaal {1} tekens."),
                                range: $.validator.format("Vul hier een waarde in van minimaal {0} en maximaal {1}."),
                                max: $.validator.format("Vul hier een waarde in kleiner dan of gelijk aan {0}."),
                                min: $.validator.format("Vul hier een waarde in groter dan of gelijk aan {0}.")
                            });
                        }(jQuery));
                        var $mcj = jQuery.noConflict(true);</script>
                    <!--End mc_embed_signup-->
                    <div class="fb">
                        <p><a class="knop" href="https://www.facebook.com/fotografiecvodeverdieping/ "
                              target="_blank"><i class="fab fa-facebook-square"></i> Fotografie cvo de verdieping</a>
                        </p>
                        <p><a class="knop" href="https://www.facebook.com/events/2014703872186457/" target="_blank"><i
                                        class="fab fa-facebook-square"></i> Dag van de fotografie</a></p>
                        <a class="knop" href="{{ route('home') }}"><i class="fas fa-arrow-left"></i> Terug naar menu</a>
                    </div>

                </div>
                <div class="possible">
                    <h2>Auto</h2>
                    <p><b>Via de snelweg van uit Genk.</b> Gebruik de E314 tot aan afrit 29 Sla rechts af tot aan de 3de
                        lichten Sla links af Herebaan-west Op de rotonde gaat u links 2de afslag De volgende rotonde
                        rechts 1ste afslag De volgende rotonde links 2de afslag De parking bevindt zich aan uw linker
                        kant</p>
                    <p><b>Via de snelweg van uit Brussel.</b> Gebruik de E314 tot aan afrit 29 Sla links af tot aan de
                        4de lichten Sla links af Herebaan-west Op de rotonde gaat u links 2de afslag De volgende rotonde
                        rechts 1ste afslag De volgende rotonde links 2de afslag De parking bevindt zich aan uw linker
                        kant</p>
                    <p><b>Via de snelweg van uit Antwerpen.</b> Gebruik de E313 aan het klaverblad van Lummen volg de
                        aantwijzing Genk. Volg de E314 tot aan afrit 29 Sla links af tot aan de 4de lichten Sla links af
                        Herebaan-west Op de rotonde gaat u links 2de afslag De volgende rotonde rechts 1ste afslag De
                        volgende rotonde links 2de afslag De parking bevindt zich aan uw linker kant</p>
                    <h2>Trein</h2>
                    <p>
                        Het station van Zolder bevindt zich op 10 min wandelen van de luchtfabriek. via de stationstraat
                        wandeld u naar de koolmijnlaan. Hierbij kan u tussen de gebouwen doorlopen.
                    </p>
                    <h2>Bus</h2>
                    <p>
                        De bushalte "zolder Pleintje" bevindt zich op 2min wandelen. U stap af een de koolmijnlaan
                        hierbij volgt u de baan. Waarbij u wederom tussen de gebouwen kan doorlopen.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ URL::asset('js/photoexpo.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <script type="text/javascript" src={{ URL::asset('js/jquery.min.js')}} ></script>
    <script type="text/javascript" src={{ URL::asset('js/jquery-1.11.1.min.js')}} ></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.textillate.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.lettering.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.fittext.js') }}"></script>
    <script type="text/javascript">
        $('h1').textillate({
            in: {effect: 'fadeInRightBig', delay: 200},
        });
    </script>
    </body>


    </html>
@endsection