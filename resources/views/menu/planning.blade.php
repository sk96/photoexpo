@extends('layouts.htmltop')
@section('content')
    <body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="logo">
                    <img src="img/dagfoto7.png"/>
                </div>
                <h1>Time schedule</h1>

                <div class="timetable"></div>

            </div>
            <div class="content">
                <h2>Extra info:</h2>
                <p><b>Een workshop of demo volgen tijdens de Dag van de fotografie? Dat kan!<br> Je neemt uiteraard je
                        camera mee, mét volledig opgeladen batterij, voldoende geheugen en objectief.</b></p>
                <p>Inschrijven voor de workshops en demo's is verplicht</p>
                <p>Om sommige workshops en demo's te kunnen aanbieden, vragen we een vergoeding van 10 euro.<br> Je
                    inschrijving is pas definitief na betaling.
                </p>
                <p>indien we dit bedrag niet tijdig ontvangen, wordt je inschrijving geannuleerd.</p>
                <p>We vragen je om ten laatste 10 minuten voor de start van de workshop/demo aanwezig te zijn.<br> Als
                    de workshop gestart is, gaan de deuren dicht.
                </p>
                <p>Lezingen zijn gratis te volgen. Hiervoor is inschrijven ook niet nodig!</p>
                <p>De beurs en de expo is doorlopend te bezoeken. Ook dit is volledig gratis!</p>

                <h4>Kleine tip</h4>
                <p>Indien je op een vak klikt of drukt krijg je meer info over dit bepaalde item, en kan je jezelf
                    inschrijven!</p>
                <div class="btntest">
                    <p><a class="knop" href="{{ route('home') }}"><i class="fas fa-arrow-left"></i> Terug naar menu</a>
                    </p>
                </div>
            </div>
            <script type="text/javascript" src="{{ URL::asset('js/timetable.min.js') }}"></script>
            <script type="text/javascript" src="{{ URL::asset('js/timetable.js') }}"></script>
            <script type="text/javascript" src={{ URL::asset('js/jquery.min.js')}} ></script>
            <script type="text/javascript" src={{ URL::asset('js/jquery-1.11.1.min.js')}} ></script>
            <script type="text/javascript" src="{{ URL::asset('js/jquery.textillate.js') }}"></script>
            <script type="text/javascript" src="{{ URL::asset('js/jquery.lettering.js') }}"></script>
            <script type="text/javascript" src="{{ URL::asset('js/jquery.fittext.js') }}"></script>
            <script type="text/javascript">

                var timetable = new Timetable();
                timetable.setScope(6, 22)
                @foreach($locations as $location)
                timetable.addLocations(['{{ $location }}']);
                @endforeach
                @foreach($events as $event)
                timetable.addEvent('{{ $event->event_name }}-{{ $event->title }}-{{ $event->name }}', '{{ $event->location }}', new Date({{$event->startdate}}), new Date({{$event->enddate}}), {url: '{{ route("detail.event", $event->id) }}'});
                        @endforeach

                var renderer = new Timetable.Renderer(timetable);
                renderer.draw('.timetable');

                $('h1').textillate({
                    in: {effect: 'fadeInRightBig', delay: 200},
                });
            </script>
        </div>
    </div>
    </div>
    </body>

    </html>
@endsection