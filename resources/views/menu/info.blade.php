@extends('layouts.htmltop')
@section('content')
    <body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="logo">
                    <img src="img/dagfoto3.png"/>
                </div>
                <h1>Opleiding fotografie</h1>
                <div class="content">
                    <div class="opleiding">
                        <h2>Over onze opleiding</h2>
                        <p>Aan het CVO De Verdieping in Heusden-Zolder in Limburg bieden we een uitgebreide opleiding
                            fotografie aan. Iedereen die wilt leren fotograferen, of nog betere foto's wil leren maken,
                            kan bij ons terecht! Les volgen kan één keer per week, maar je kan er ook kiezen voor het
                            sneltraject, waarbij je meerdere keren per week les volgt.</p>
                        <p>De cursus fotografie bestaat uit de vierdelige pro-reeks, waarin je in de basis van de
                            fotografie wordt ondergedompeld. Je leert de geheimen van je spiegelreflexcamera kennen en
                            zet je eerste stappen binnen de studiofotografie. Het vervolg van deze reeks is de
                            themagerichte projectreeks. De opdrachten zijn creatiever en intensiever. De projecten
                            draaien rond diepgang - we steken meer tijd in elke opdracht waardoor de resulterende
                            beelden naar een hoger niveau getild worden.</p>
                        <p>Hiernaast bieden we ook nog de nabewerkingmodules Lightroom en Photoshop voor fotografen aan,
                            waarin we de kwaliteit van onze beelden extra in de verf leren zetten. Verscherpen,
                            ophelderen, retoucheren, kleuren corrigeren, in zwart-wit omzetten,... zijn enkele thema's
                            die aan bod komen. We raden je wel aan om de nabewerking zeker voor de projectreeks in de
                            vingers te hebben.</p>
                        <p>Tenslotte kan je in de masterclasses je helemaal verdiepen in landschapsfotografie,
                            zwart-witfotografie of flitsfotografie.</p>
                        <p>Neem voor meer informatie en het huidige lessenrooster zeker een kijkje op de website van CVO
                            De Verdieping.
                        </p>
                    </div>
                    <div class="links">
                        <h2>Voor meer informatie en inschrijvingen.</h2>
                        <p><a class="knop" target="_blank" href="http://www.cvodeverdieping.be"><i
                                        class="fas fa-camera-retro"></i> CVO De verdieping</a></p>
                        <p><a class="knop" target="_blank" href="http://vschool.be/cursussen"><i
                                        class="fas fa-camera-retro"></i> Vschool cursussen</a></p>
                        <div class="btntest">
                            <p><a class="knop" href="{{ route('home') }}"><i class="fas fa-arrow-left"></i> Terug naar
                                    menu</a></p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <script type="text/javascript" src={{ URL::asset('js/jquery.min.js')}} ></script>
    <script type="text/javascript" src={{ URL::asset('js/jquery-1.11.1.min.js')}} ></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.textillate.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.lettering.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.fittext.js') }}"></script>
    <script type="text/javascript">
        $('h1').textillate({
            in: {effect: 'fadeInRightBig', delay: 200},
        });
    </script>
    </body>
    </html>
@endsection