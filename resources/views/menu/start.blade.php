@extends('layouts.htmltop')
@section('content')
    <body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="logo">
                    <img src="img/dagfotocvo.png"/>
                </div>

                <h1>Dag van de fotografie</h1>
                <h4>Zaterdag 16/06/2018</h4>
                <div class="content">
                    <div class="explain">
                        <h2>Over deze prachtige dag</h2>
                        <p>  Ook dit jaar is het weer zover, het is reeds de 5de editie van dit prachtige evenement. Je kan wederom op een interactieve manier de wereld van de fotografie
                            ontdekken, aan de hand van lezingen, workshops, demonstraties en een beurs. Dat zegt je wel
                            wat? Kom dan zeker eens langs. Iedereen is welkom van 10:00u tot 17:00u.
                        </p>
                    </div>
                    <div class="li">
                        <h2>Wat kan u verwachten?</h2>
                        <ul>
                            <li><i class="fas fa-camera-retro"></i> Interessante demo's, workshops en lezingen</li>
                            <li><i class="fas fa-camera-retro"></i> Een heuse expositie</li>
                            <li><i class="fas fa-camera-retro"></i> Een fantastische beurs</li>
                            <li><i class="fas fa-camera-retro"></i> Een prachtige tombola (hoofdprijs een bon t.w.v
                                300€)
                            </li>
                        </ul>
                    </div>
                    <div class="tombola">
                        <h2>Tombola</h2>
                        <p><b>Waag je kans met onze fotografie tombola!</b> Ook dit jaar zijn er weer super leuke
                            tombolaprijzen te winnen!</p>
                        <p> Hoofdprijs : Waardebon van 300€
                            <br> Andere prijzen: waardebonnen van € 100, waardebonnen van € 50 ,....</p>
                        <p>Voor de aankoop van tombolaloten moet je ook bij de infostand van CVO De Verdieping
                            zijn. </p>

                        <div class="explain">
                            <h2>Cafetaria</h2>
                            <p>Honger of dorst gekregen?</p>
                            <p>Om de honger en dorst te stillen kan je terecht in de cafetaria voor sandwiches, taart en drank.</p>
                        </div>

                        <div class="btntest">
                            <p><a class="knop" href="{{ route('home') }}"><i class="fas fa-arrow-left"></i> Terug naar
                                    menu</a></p>
                        </div>
                    </div>

                </div>
                <div class="images">
                    <img class="Slider " src="img/mijn.jpg" width="300 " height="500 ">
                    <img class="Slider " src="img/image1.png " width="300 " height="500 ">
                    <img class="Slider " src="img/image10.jpg " width="300 " height="500 ">
                    <img class="Slider " src="img/image4.jpg " width="300 " height="500 ">
                    <img class="Slider " src="img/image11.jpg " width="300 " height="500 ">
                    <img class="Slider " src="img/portret.jpg " width="300 " height="500 ">
                    <img class="Slider " src="img/jong.jpg " width="300 " height="500 ">
                </div>

            </div>

        </div>
    </div>
    <script type="text/javascript" src="{{ URL::asset('js/sliderhome.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <script type="text/javascript" src={{ URL::asset('js/jquery.min.js')}} ></script>
    <script type="text/javascript" src={{ URL::asset('js/jquery-1.11.1.min.js')}} ></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.textillate.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.lettering.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.fittext.js') }}"></script>
    <script type="text/javascript">
        $('h1').textillate({
            in: {effect: 'fadeInRightBig', delay: 200},
        });
    </script>

    </body>

    </html>
@endsection