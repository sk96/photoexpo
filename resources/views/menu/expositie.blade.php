@extends('layouts.htmltop')
@section('content')
    <body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="logo">
                    <img src="img/dagfoto4.png"/>
                </div>
                <div class="titel">
                    <h1>Expositie &amp; beurs</h1>
                </div>
                <div class="slideshow-container">
                    <div class="mySlides">
                        <div class="numbertext">1 / 9</div>
                        <img src="img/mensen.jpg" style="width:100%">
                    </div>
                    <div class="mySlides">
                        <div class="numbertext">2 / 9</div>
                        <img src="img/expo1.jpg" style="width:100%">
                    </div>
                    <div class="mySlides">
                        <div class="numbertext">3/ 9</div>
                        <img src="img/expo2.jpg" style="width:100%">
                    </div>
                    <div class="mySlides">
                        <div class="numbertext">4/ 9</div>
                        <img src="img/expo3.jpg" style="width:100%">
                    </div>
                    <div class="mySlides">
                        <div class="numbertext">5 / 9</div>
                        <img src="img/image13.jpg" style="width:100%">
                    </div>
                    <div class="mySlides">
                        <div class="numbertext">6 / 9</div>
                        <img src="img/image12.jpg" style="width:100%">
                    </div>
                    <div class="mySlides">
                        <div class="numbertext">7 / 9</div>
                        <img src="img/image13.jpg" style="width:100%">
                    </div>
                    <div class="mySlides">
                        <div class="numbertext">8/ 9</div>
                        <img src="img/image14.jpg" style="width:100%">
                    </div>
                    <div class="mySlides">
                        <div class="numbertext">9/ 9</div>
                        <img src="img/image15.jpg" style="width:100%">
                    </div>
                    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                    <a class="next" onclick="plusSlides(1)">&#10095;</a>
                </div>
                <br>
                <div style="text-align:center">
                    <span class="dot" onclick="currentSlide(1)"></span>
                    <span class="dot" onclick="currentSlide(2)"></span>
                    <span class="dot" onclick="currentSlide(3)"></span>
                    <span class="dot" onclick="currentSlide(5)"></span>
                    <span class="dot" onclick="currentSlide(6)"></span>
                    <span class="dot" onclick="currentSlide(7)"></span>
                    <span class="dot" onclick="currentSlide(7)"></span>
                    <span class="dot" onclick="currentSlide(8)"></span>
                    <span class="dot" onclick="currentSlide(9)"></span>
                </div>
                <div>
                    <h1>Expositie</h1>
                    <p>Je kan in de gangen van CVO De Verdieping de afstudeerprojecten van onze cursisten 'Project 5 –
                        portfolio’, schooljaar 2017-2018 bewonderen. Deze cursisten werkten een hele module aan een
                        eigen project. Zeker de moeite om een kijkje te nemen dus.</p>
                </div>
                <div>
                    <h1>Beurs</h1>
                    <p>In ZLDR Luchtfabriek kan je van 10:00 tot 17:00 een bezoekje brengen aan de fotografiebeurs. Er
                        zijn veel standhouders van de partij, die je graag meer uitleg geven over hun aanbod.</p>
                    <div class="imgs">
                        <a href="http://bartokshop.be" target="_blank"><img src="img/Bartok.png" alt="bartok"></a>
                    </div>
                    <div class="images">
                        <a href="https://www.nikon.be/fr_BE/" target="_blank"><img src="img/Nikon.png" alt="nikon"></a>
                    </div>
                    <div class="imgs">
                        <a href="http://www.fujifilm.com/" target="_blank"><img src="img/Fujifilm.png"
                                                                                alt="fujifilm"></a>
                    </div>
                    <div class="images">
                        <a href="https://www.canon.be/" target="_blank"><img src="img/Canon.png" alt="canon"></a>
                    </div>
                    <div class="imgs">
                        <a href="https://www.sigmabenelux.com/" target="_blank"><img src="img/Sigma.png"
                                                                                     alt="sigmabenelux"></a>
                    </div>
                    <div class="images">
                        <a href="https://www.sony.be/nl/electronics/cameras" target="_blank"><img src="img/Sony.png"
                                                                                                  alt="sony"></a>
                    </div>
                    <div class="imgs">
                        <a href="http://shop.panasonic.be/" target="_blank"><img src="img/Panasonic.png"
                                                                                 alt="panasonic"></a>
                    </div>
                    <div class="imgs">
                        <div class="btntest">
                            <p><a class="knop" href="{{ route('home') }}"><i class="fas fa-arrow-left"></i> Terug naar
                                    menu</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{ URL::asset('js/sliderexpo.js') }}"></script>
    <script type="text/javascript" src={{ URL::asset('js/jquery.min.js')}} ></script>
    <script type="text/javascript" src={{ URL::asset('js/jquery-1.11.1.min.js')}} ></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.textillate.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.lettering.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.fittext.js') }}"></script>
    <script type="text/javascript">
        $('h1').textillate({
            in: {effect: 'fadeInRightBig', delay: 200},
        });
    </script>
    </body>

    </html>
@endsection