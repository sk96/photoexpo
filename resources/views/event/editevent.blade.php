@extends('layouts.htmltop')
@section('content')
<body>
<div class="container">
    <div class="huls">
        <div class="form-group">
            <h3>Pas een event aan</h3>
        </div>
        <form action="{{ route('update.event', $event->id) }}" method="post">
            @include('shared.errors')
            {{ csrf_field() }}
            <div class="form-group">
                <label for="event_name">Soort event</label>
                <input type="text" class="form-control" name="event_name" id="event_name" value="{{ $event->event_name }}"
                       placeholder="Vul soort event in...">
            </div>

            <div class="form-group">
                <label for="title">Titel</label>
                <input type="text" class="form-control" name="title" id="title" value="{{ $event->title }}"
                       placeholder="Vul titel event toe...">
            </div>
            <div class="form-group">
                <label for="name">Naam</label>
                <input type="text" class="form-control" name="name" id="name" value="{{ $event->name }}"
                       placeholder="Vul naam docent toe...">
            </div>
            <div class="form-group">
                <label for="location">Locatie</label>
                <input type="text" class="form-control" name="location" id="location" value="{{ $event->location }}"
                       placeholder="Waar vind het event plaats...">
            </div>
            <div class="form-group">
                <label for="startdate">Startuur</label>
                <input type="text" class="form-control" name="startdate" id="startdate" value="{{ $event->startdate }}"
                       placeholder="Vul in datum en startuur : 2018, 6, 16, 15, 45, 00">
            </div>
            <div class="form-group">
                <label for="enddate">Einduur</label>
                <input type="text" class="form-control" name="enddate" id="enddate" value="{{ $event->enddate }}"
                       placeholder="Vul in datum en einduur : 2018, 6, 16, 16, 45, 00">
            </div>
            <div class="form-group">
                <label for="price">Prijs</label>
                <input type="text" class="form-control" name="price" id="price" value="{{ $event->price }}"
                       placeholder="Vul prijs in, indien gratis 0...">
            </div>
            <div class="form-group">
                <label for="quantity">Aantal personen</label>
                <input type="text" class="form-control" name="quantity" id="quantity" value="{{ $event->quantity }}"
                       placeholder="Geef max aantal personen in...">
            </div>
            <div class="form-group">
                <label for="content">Inhoud</label>
                <input type="text" class="form-control" name="content" id="content" value="{{ $event->content }}"
                       placeholder="Waarover gaat het event...">
            </div>
            <div class="form-group">
                <label for="bio">Biografie</label>
                <input type="text" class="form-control" name="bio" id="bio" value="{{ $event->bio }}"
                       placeholder="Geef hier info over de docent toe...">
            </div>

            <div class="form-group">
                <button type="submit" class="knop">Voeg toe</button>
                <a class="knop" href="{{ route('admin') }}">Admin pagina</a>
            </div>
        </form>
    </div>
</div>
</div>
</body>
</html>
@endsection