@extends('layouts.htmltop')
@section('content')

    <body>
    <div class="container">
        <div class="row">

            <div class="logo">
                <img src="{{ URL::asset('img/cvo.png') }}"/>


                <hr>
                <h1>{{ $event->event_name }} {{ $event->title }} - {{ $event->name }}</h1>
                <hr>

            </div>
            <div class="col-10">
                <h2>Waar gaat dit door?</h2>
                <p class="tabel">{{ $event->location }}</p>
                <h2>Wat beleven we hier?</h2>
                <p class="tabel">
                    {{ $event->content }}
                </p>

            </div>
            <div class="col col-6">
                <h2>Door wie?</h2>
                <p>{{ $event->bio }}</p>
            </div>
            <aside class=" col col-4 praktisch">
                <h2>Praktisch</h2>
                <ul class="inline">
                    <li><p><strong>Prijs:</strong>{{ $event->price }} €</p></li>

                    <li><p><strong>Deelnemers:</strong>{{ $event->quantity }} personen max.</p></li>
                </ul>
            </aside>
        </div>
        <a class="knop" href="{{ route('show.event') }}">Back</a>
        <hr>

        @if ($event->event_name == "Workshop" || $event->event_name == "Demo")
            <?php
            $now = date('Y-m-d') . "\n";
            // dd($now);
            $datum = '2018-05-27';
            //dd($datum);
            ?>
            @if($now <= $datum )
                    <a class="knop" href="{{ route('token.event', $event->id) }}">Inschrijving event</a>
            @else
        <a class="knop" href="{{ route('order.event', $event->id) }}">Inschrijving event</a>
            @endif
        @endif
        @if(Auth::check()&& Auth::id('admin'))
            <hr>
            <a class="knop" href="{{ route('edit.event', $event->id) }}">Aanpassen event</a>
            <hr>
            <a class="knop" href="{{ route('delete.event', $event->id) }}">Verwijder event</a>
            <hr>
            @if ($event->event_name == "Workshop" || $event->event_name == "Demo")
            <a class="knop" href="{{ route('listorder.order', $event->id) }}">Lijst order</a>
            <hr>
                @endif
        @endif
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    </body>

    </html>
@endsection