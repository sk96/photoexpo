@extends('layouts.htmltop')
@section('content')
    <body>
    <div class="container">
        <div class="huls">
            <div class="form-group">
                <h3>Geldigheid token</h3>
            </div>
            <form action="{{ route('showtoken.order') }}" method="post">
                @include('shared.errors')
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="token">Token</label>
                    <input type="text" class="form-control" name="token" value="token" id="token" aria-describedby="nameHelp"
                           placeholder="Invoeren token">
                </div>
                <div class="form-group">
                    <button type="submit" class="knop">Verifïeer token</button>
                </div>
            </form>
       <hr>
       <a class="knop" href="{{ route('home') }}"><i class="fas fa-arrow-left"></i> Terug naar planning</a>
        </div>
    </div>
    </body>
@endsection