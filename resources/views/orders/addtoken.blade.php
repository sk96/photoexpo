@extends('layouts.htmltop')
@section('content')
    <body>
    <div class="container">
        <div class="huls">
            <div class="form-group">
                <h3>Invoegen nieuwe tokens</h3>
            </div>
            <form action="{{ route('savetoken.order') }}" method="post">
                @include('shared.errors')
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="token">Token</label>
                    <input type="text" class="form-control" name="token" id="token" aria-describedby="nameHelp"
                           placeholder="Invoeren token">
                </div>
                <div class="form-group">
                    <button type="submit" class="knop">Schrijf in</button>
                </div>
            </form>
                <ul>
                    @foreach($tokens as $token)
                    <li>{{ $token->token }}</li>
                        @endforeach
                </ul>

            <a class="knop" href="{{ ('admin') }}">Back</a>
        </div>
    </div>
    </div>
    </body>
    </html>
@endsection