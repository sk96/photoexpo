@extends('layouts.htmltop')
@section('content')
    <body>
    <div class="container">
        <div class="huls">
            <div id="printableArea">
                <h1>Aantal inschrijvingen voor workshop</h1>
                <ul>
                    @foreach($orders as $order)
                        <li>{{ $order->name }}</li>
                        <li>{{ $order->firstname }}</li>
                        <li>{{ $order->email }}</li>
                    @endforeach
                </ul>
            </div>
            <input class="knop" type="button" onclick="printDiv('printableArea')" value="print a div!" />
            <a class="knop" href="{{ ('admin') }}">Back</a>
        </div>
    </div>
    </div>
    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
    </body>
    </html>
@endsection