@extends('layouts.htmltop')
@section('content')
    <body>
    <div class="container">
        <div class="huls">
            <div class="form-group">

                <h3>Inschrijving</h3>
            </div>
            @foreach($events as $event)
            <form action="{{ route('save.order', $event->id) }}" method="post">
                @endforeach
                @include('shared.errors')
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Naam</label>
                    <input type="text" class="form-control" name="name" id="name" aria-describedby="nameHelp"
                           placeholder="Naam">
                </div>
                <div class="form-group">
                    <label for="name">Voornaam</label>
                    <input type="text" class="form-control" name="firstname" id="firstname" aria-describedby="nameHelp"
                           placeholder="Voornaam">
                </div>

                <div class="form-group">
                    <label for="Email">Email adres</label>
                    <input type="email" class="form-control" name="email" id="Email1" aria-describedby="emailHelp"
                           placeholder="Email">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.
                    </small>
                </div>

                <div class="form-group">
                    <button type="submit" class="knop">Schrijf in</button>
                </div>
            </form>

            <a class="knop" href="{{ ('planning') }}">Back</a>
        </div>
    </div>
    </div>
    </body>
    </html>
@endsection