<!DOCTYPE html>
<html lang="nl">

<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Condensed:100">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <script type="text/javascript" src="{{ URL::asset('js/photoexpo.js') }}"></script>
</head>

<body>
<div class="split-slideshow">
    <div class="slideshow">
        <div class="slider">
            <div class="item responsive">
                <a href="{{ route('start') }}"><img src="img/Workshops1.png" /></a>
            </div>
            <div class="item">
                <a href="{{ route('show.event') }}"><img src="img/workshop.jpg" /></a>
            </div>
            <div class="item">
                <a href="{{ route('expositie') }}"><img src="img/mensen.jpg" /></a>
            </div>
            <div class="item">
                <a href="{{ route('inschrijving') }}"><img src="img/foto1.jpg" /></a>
            </div>
            <div class="item">
                <a href="{{ route('info') }}"><img src="img/auto.jpg" /></a>
            </div>
            <div class="item">
                <a href="{{ route('contact') }}">Contact<img src="img/Beurs.jpg" /></a>
            </div>
        </div>
    </div>
    <div class="slideshow-text">
        <div class="item menu">Foto-event</div>
        <div class="item menu">Planning</div>
        <div class="item menu">Expo</div>
        <div class="item menu">Inschrijving</div>
        <div class="item menu ">Info</div>
        <div class="item menu">Contact</div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.12/jquery.mousewheel.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
<script>
    window.jQuery || document.write('<script src="js/jquery-3.2.1.min.js"><\/script>')
</script>
<script src="js/photoexpo.js"></script>
</body>

</html>