@extends('layouts.htmltop')
@section('content')
<body>
<div class="container">
    <div class="huls">
        <div class="form-group">
            <h3>Register</h3>
        </div>
        <form action="{{ route('register.post') }}" method="post">
            @include('shared.errors')
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name">Naam</label>
                <input type="text" class="form-control" name="name" id="name" aria-describedby="nameHelp"
                       placeholder="Enter name">
            </div>

            <div class="form-group">
                <label for="Email">Email adres</label>
                <input type="email" class="form-control" name="email" id="Email1" aria-describedby="emailHelp"
                       placeholder="Enter email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.
                </small>
            </div>
            <div class="form-group">
                <label for="Password">Paswoord</label>
                <input type="password" class="form-control" name="password" id="Password" placeholder="Password">
            </div>
            <div class="form-group">
                <label for="Password">Bevestig paswoord</label>
                <input type="password" class="form-control" name="password_confirmation" id="Password_confirmation"
                       placeholder="Confirm password">
            </div>
            <div class="form-group">
                <button type="submit" class="knop">Registreer</button>
                <a class="knop" href="{{ route('admin') }}">Admin pagina</a>
            </div>
        </form>
    </div>
</div>
</div>
</body>
</html>
@endsection