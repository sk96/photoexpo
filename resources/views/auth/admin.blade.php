@extends('layouts.htmltop')
@section('content')

    <body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="logo">
                    <img src="img/cvo.png"/>
                </div>
                <h1>Admin Pagina</h1>
                @if(Auth::check()&& Auth::id('admin'))
                    <a class="knop" href="{{ route('logout') }}">Log out</a>
                    <hr>
                    <a class="knop" href="{{ route('add.event') }}">Aanmaken event</a>
                    <hr>
                    <a class="knop" href="{{ ('planning') }}">Kies event en pas aan</a>
                    <hr>
                    <a class="knop" href="{{ ('planning') }}">Kies event en verwijder</a>
                    <hr>
                    <a class="knop" href="{{ route('addtoken.order') }}">Invoegen nieuwe tokens</a>
                    <hr>
                    <a class="knop" href="{{ route('deletetoken.order') }}">Wissen tokens</a>
                    <hr>
                    <a class="knop" href="{{ ('planning') }}">Lijst events</a>
                    <hr>
                    <a class="knop" href="{{ route('delete.event') }}">Lijst alle events wissen</a>
                    <hr>

                @else
                    <a class="knop" href="{{ route('login') }}">Log in</a>
                    <hr>
                    <a class="knop" href="{{ route('register') }}">Registreer</a>
                    <hr>
                @endif
                <a class="knop" href="{{ route('start') }}">Home pagina</a>
            </div>

        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    </body>

    </html>
@endsection