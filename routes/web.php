<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index')->name('home');

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login')->name('login.post');

Route::get('/logout', 'Auth\loginController@logout')->name('logout');

Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register', 'Auth\RegisterController@register')->name('register.post');

Route::get('/contact', 'PageController@contact')->name('contact');
Route::get('/start', 'PageController@start')->name('start');
Route::get('/expositie', 'PageController@expositie')->name('expositie');
Route::get('/info', 'PageController@info')->name('info');
Route::get('/admin', 'PageController@admin')->name('admin');
Route::get('/inschrijving', 'PageController@inschrijving')->name('inschrijving');


Route::get('/planning', 'EventController@planning')->name('planning');
Route::get('/planning', 'EventController@showEvent')->name('show.event');

Route::get('/add', 'EventController@addEvent')->name('add.event');
Route::post('/planning', 'EventController@saveEvent')->name('save.event');

Route::get('{id}/edit', 'EventController@editEvent')->name('edit.event');
Route::post('{id}/planning', 'EventController@updateEvent')->name('update.event');

Route::get('{id}/delete', 'EventController@deleteEvent')->name('delete.event');

Route::get('{id}/event', 'EventController@detailEvent')->name('detail.event');

Route::get('/add_token', 'OrderController@addToken')->name('addtoken.order');
Route::post('/admin', 'OrderController@saveToken')->name('savetoken.order');

Route::get('/delete', 'OrderController@deleteToken')->name('deletetoken.order');
Route::post('/token', 'OrderController@showToken')->name('showtoken.order');

Route::post('{id}/order','OrderController@saveOrder')->name('save.order');
Route::get('{id}/list','OrderController@listOrder')->name('listorder.order');

Route::get('{id}/token', 'OrderController@tokenEvent')->name('token.event');
Route::get('{id}/order', 'OrderController@orderEvent')->name('order.event');
Route::get('/delete_event', 'EventController@deleteAllEvents')->name('delete.event');




