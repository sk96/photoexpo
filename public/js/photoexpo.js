    $(document).ready(function() {
        var $slider = $('.slideshow .slider'),
            maxItems = $('.item', $slider).length,
            dragging = false,
            tracking,
            rightTracking;

        $sliderRight = $('.slideshow').clone().addClass('slideshow-right').appendTo($('.split-slideshow'));

        rightItems = $('.item', $sliderRight).toArray();
        reverseItems = rightItems.reverse();
        $('.slider', $sliderRight).html('');
        for (i = 0; i < maxItems; i++) {
            $(reverseItems[i]).appendTo($('.slider', $sliderRight));
        }

        $slider.addClass('slideshow-left');
        $('.slideshow-left').slick({
            vertical: true,
            verticalSwiping: true,
            arrows: false,
            infinite: true,
            dots: true,
            speed: 1000,
            cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)'
        }).on('beforeChange', function(event, slick, currentSlide, nextSlide) {

            if (currentSlide > nextSlide && nextSlide == 0 && currentSlide == maxItems - 1) {
                $('.slideshow-right .slider').slick('slickGoTo', -1);
                $('.slideshow-text').slick('slickGoTo', maxItems);
            } else if (currentSlide < nextSlide && currentSlide == 0 && nextSlide == maxItems - 1) {
                $('.slideshow-right .slider').slick('slickGoTo', maxItems);
                $('.slideshow-text').slick('slickGoTo', -1);
            } else {
                $('.slideshow-right .slider').slick('slickGoTo', maxItems - 1 - nextSlide);
                $('.slideshow-text').slick('slickGoTo', nextSlide);
            }
        }).on("mousewheel", function(event) {
            event.preventDefault();
            if (event.deltaX > 0 || event.deltaY < 0) {
                $(this).slick('slickNext');
            } else if (event.deltaX < 0 || event.deltaY > 0) {
                $(this).slick('slickPrev');
            };
        }).on('mousedown touchstart', function() {
            dragging = true;
            tracking = $('.slick-track', $slider).css('transform');
            tracking = parseInt(tracking.split(',')[5]);
            rightTracking = $('.slideshow-right .slick-track').css('transform');
            rightTracking = parseInt(rightTracking.split(',')[5]);
        }).on('mousemove touchmove', function() {
            if (dragging) {
                newTracking = $('.slideshow-left .slick-track').css('transform');
                newTracking = parseInt(newTracking.split(',')[5]);
                diffTracking = newTracking - tracking;
                $('.slideshow-right .slick-track').css({ 'transform': 'matrix(1, 0, 0, 1, 0, ' + (rightTracking - diffTracking) + ')' });
            }
        }).on('mouseleave touchend mouseup', function() {
            dragging = false;
        });

        $('.slideshow-right .slider').slick({
            swipe: false,
            vertical: true,
            arrows: false,
            infinite: true,
            speed: 950,
            cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
            initialSlide: maxItems - 1
        });
        $('.slideshow-text').slick({
            swipe: false,
            vertical: true,
            arrows: false,
            infinite: true,
            speed: 900,
            cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)'
        });

    });


    var voornaamTxt;
    var achternaamTxt;
    var emailTxt;
    var telefoonnummerTxt;
    var textTxt;

    var allesCorrectIngevuld = true;

    function controleerVoorwaardenVoornaam() {
        if ((voornaamTxt).length < 2) {
            document.getElementById("voornaam_error").innerHTML = "Minstens 2 karakters lang!";
            allesCorrectIngevuld = false;
        } else {
            document.getElementById("voornaam_error").innerHTML = "";
        }
    }

    function controleerVoorwaardenAchternaam() {
        if ((achternaamTxt).length < 2) {
            document.getElementById("achternaam_error").innerHTML = "Minstens 2 karakters lang!";
            allesCorrectIngevuld = false;
        } else {
            document.getElementById("achternaam_error").innerHTML = "";
        }
    }

    function controleerVoorwaardenEmail() {
        var regExp = /^[A-Za-z]([\.A-Za-z0-9+_-])+@([\.A-Za-z0-9-])+\.[A-Za-z]{2,20}$/;
        if (regExp.test(emailTxt) == false) { // test = true of false 
            document.getElementById("email_error").innerHTML = "Dit is niet correct!";
            allesCorrectIngevuld = false;
        } else {
            document.getElementById("email_error").innerHTML = "";
        }
    }

    function controleerVoorwaardentelefoonnummer() {
        var regExp = /^((\+|00)32\s?|0)4(60|[789]\d)(\s?\d{2}){3}$/;
        if (regExp.test(telefoonnummerTxt) == false) {
            document.getElementById("telefoonnummer_error").innerHTML = "Dit is niet correct!";
            allesCorrectIngevuld = false;
        } else {
            document.getElementById("telefoonnummer_error").innerHTML = "";
        }
    }

    function verstuur() {
        voornaamTxt = document.getElementById("voornaam").value;
        achternaamTxt = document.getElementById("achternaam").value;
        emailTxt = document.getElementById("email").value;
        telefoonnummerTxt = document.getElementById("telefoonnummer").value;
        textTxt = document.getElementById("mijnTekst").value;

        allesCorrectIngevuld = true;


        if (voornaamTxt.length == 0) {
            document.getElementById("voornaam_error").innerHTML = "Vul in a.u.b.";
            allesCorrectIngevuld = false;
        } else {
            //aparte functie aanmaken om de voorwaarden te controleren
            controleerVoorwaardenVoornaam();
        }


        if (achternaamTxt.length == 0) {
            document.getElementById("achternaam_error").innerHTML = "Vul in a.u.b.";
            allesCorrectIngevuld = false;
        } else {
            //aparte functie aanmaken om de voorwaarden te controleren
            controleerVoorwaardenAchternaam();
        }

        if (emailTxt.length == 0) {
            document.getElementById("email_error").innerHTML = "Vul in a.u.b.";
            allesCorrectIngevuld = false;
        } else {
            controleerVoorwaardenEmail();
        }

        if (telefoonnummerTxt.length == 0) {
            document.getElementById("telefoonnummer_error").innerHTML = "Vul in a.u.b.";
            allesCorrectIngevuld = false;
        } else {
            controleerVoorwaardentelefoonnummer();
        }

        if (allesCorrectIngevuld) {
            console.log("Bedankt voor uw bericht.");
            var link = "mailto:kuypers.sven96@gmail.com" +

                "&subject=" + encodeURIComponent("Formulier validatie") +
                "&body=" +
                "Voornaam: " +
                encodeURIComponent(voornaamTxt) +
                encodeURIComponent("\r\n\n") +
                "Achternaam: " +
                encodeURIComponent(achternaamTxt) +
                encodeURIComponent("\r\n\n") +
                "Straatnaam: " +
                encodeURIComponent(emailTxt) +
                encodeURIComponent("\r\n\n") +
                "Telefoonnummer: " +
                encodeURIComponent(telefoonnummerTxt) +
                encodeURIComponent("\r\n\n") +
                "Tekst: " +
                encodeURIComponent(textTxt) +
                encodeURIComponent("\r\n\n")

            window.location.href = link;

        }
    }

    