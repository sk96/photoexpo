<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'event_name', 'title', 'name', 'location', 'startdate', 'enddate', 'price', 'quantity', 'content', 'bio'
    ];
    public function token()
    {
        return $this->hasOne('App\Token', 'id');
    }

    public function orders()
    {
        return $this->hasMany('App\Order', 'id');
    }
}
