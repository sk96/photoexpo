<?php

namespace App\Http\Controllers;
use App\User;
use App\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{

    public function detailEvent($id)
    {
        $event = Event::find($id);
        return view('event.event', compact('event'));
    }
    public function planning()
    {
        return view('menu.planning');
    }
    public function showEvent()
    {
        $events = Event::all();


        $locations = array();
        foreach($events as $event){
            $locations[] = $event->location;
        }
        $locations = array_unique($locations);


        return view('menu.planning', compact('events','locations'));
    }
    public function addEvent(){

        return view('event.addevent', compact('event'));

    }
    public function saveEvent(Request $request){
        $events = Event::all();
        $event = new Event;
        $event->event_name = $request->input('event_name');
        $event->title = $request->input('title');
        $event->name = $request->input('name');
        $event->location = $request->input('location');
        $locations[] = $event->location;
        $event->startdate = $request->input('startdate');
        $event->enddate = $request->input('enddate');
        $event->price = $request->input('price');
        $event->quantity = $request->input('quantity');
        $event->content = $request->input('content');
        $event->bio = $request->input('bio');
        $event->save();
        return redirect()->route('home');

    }
    public function editEvent($id)
    {
        $event = Event::find($id);
        return view('event.editevent', compact('event'));
    }
    public function updateEvent($id, Request $request){
        $events = Event::all();
        $event = new Event;
        $event->event_name = $request->input('event_name');
        $event->title = $request->input('title');
        $event->name = $request->input('name');
        $event->location = $request->input('location');
        $locations[] = $event->location;
        $event->startdate = $request->input('startdate');
        $event->enddate = $request->input('enddate');
        $event->price = $request->input('price');
        $event->quantity = $request->input('quantity');
        $event->content = $request->input('content');
        $event->bio = $request->input('bio');
        $event->save();
        return redirect()->route('home');

    }

    public function deleteEvent($id){
        $event = Event::find($id);
        $event->delete();
        return redirect()->route('home');

    }
    public function deleteAllEvents()
    {
        Event::all();
        Event::truncate();
        return view('auth.admin');
    }


}
