<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
        return view('index');
    }
    public function contact()
    {
        return view('menu.contact');
    }
    public function info()
    {
        return view('menu.info');
    }
    public function start()
    {
        return view('menu.start');
    }
    public function expositie()
    {
        return view('menu.expositie');
    }
    public function inschrijving()
    {
        return view('menu.inschrijving');
    }
    public function admin()
    {
        $user = User::all();
        return view('auth.admin', compact('user'));
    }
}
