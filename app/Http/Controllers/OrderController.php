<?php

namespace App\Http\Controllers;
use App\Order;
use App\Token;
use App\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\registration;

class OrderController extends Controller
{
    public function orderEvent($id)
    {
        $event = Event::find($id);
        return view('orders.order', compact('event'));
    }

    public function tokenEvent($id)
    {
        $event = Event::find($id);
        $tokens = Token::all();
        return view('orders.token', compact('event', 'tokens'));
    }

    public function addToken()
    {
        $tokens = Token::all();
        return view('orders.addtoken', compact('tokens'));
    }

    public function saveToken(Request $request)
    {
        $token = new Token;
        $token->token = $request->input('token');
        $token->save();

        return redirect()->route('addtoken.order');
    }

    public function deleteToken()
    {
        Token::all();
        Token::truncate();
        return view('auth.admin');
    }

    public function showToken()
    {
        $events = Event::all();
        $tokens = Token::all();
        foreach ($tokens as $token) {
            $nieuwtoken = $_POST['token'];
            //dd($nieuwtoken);
            $dbtoken = $token->token;
            //dd($dbtoken);
        }
            if (!($nieuwtoken == $dbtoken)) {

                echo "<div class='praktisch'><p>Je token is jammer genoeg niet geldig,</p><p>deze komt niet overeen met onze gegevens of werd reeds gebruikt !!</p>
               <p>Je kan nog inschrijven na 27 mei 2018, wanneer iedereen toegang heeft tot de inschrijvingen.</p>
                <p>Alvast bedankt voor de interesse en tot ziens.</p></div>";

                return view('orders.comment');


            } else {

                return view('orders.order', compact('tokens', 'events'));
            }

    }

    public function saveOrder($id, Request $request)
    {
        $event = Event::find($id);

        $order = new Order;
        $order->event_id = $event->id;
        $order->name = $request->input('name');
        $order->firstname = $request->input('firstname');
        $order->email = $request->input('email');
        $event->save();
        $order->save();
        Mail::to($order)->send(new Registration($order));

    }
    public function listOrder($id)
    {
        $event = Event::find($id);
        $events = Event::all();
        $order = Order::find($id);
        $orders = Order::all();
        $order->event_id = $event->id;
        return view('orders.showorders', compact('orders','event', 'events'));

    }

}
