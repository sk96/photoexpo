<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['event_id', 'name', 'firstname', 'email', 'total'];

    public function event()
    {
        return $this->belongsTo('App\Event', 'id', 'event_id');
    }
}
