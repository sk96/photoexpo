<?php
use App\Token;
use App\Event;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tokens')->truncate();
        DB::table('events')->truncate();


        $token = new Token;
        $token->token="123456abcd";
        $token->completed=false;
        $token->save();

        $token = new Token;
        $token->token="654321abcd";
        $token->completed=false;
        $token->save();

        $token = new Token;
        $token->token="102030abcd";
        $token->completed=false;
        $token->save();

        $token = new Token;
        $token->token="456789abcd";
        $token->save();

        $event = new Event;
        $event->event_name="Workshop";
        $event->title="macro fotografie";
        $event->name="Steve de Waele";
        $event->location= "studio-gebied1";
        $event->startdate="2018, 6, 16, 10, 30, 00";
        $event->enddate="2018, 6, 16, 12, 30, 00";
        $event->price="10";
        $event->quantity="14";
        $event->content="Welke materialen wordt er gebruikt bij macrofotografie.
Hoe ga je te werk
Licht beheersen en flitslicht controleren.

Voor deze workshops kan je best het volgende materiaal meebrengen ( niet verplicht alles te hebben)
Camerastatief 
Flits + swivel (indien je hierover beschikt)
Camera 
Macro lens
Witte paraplu of zwarte paraplu
Tussenringen 
Afstandsbediening 
70-200 ( ideaal macro objectief om van op afstand te werken)
Zender + ontvanger (indien je hierover beschikt)
";
        $event->bio="Wonende in Gent, gehuwd met Cindy en samen hebben wij drie kinderen. 
Zelfstandig sinds 1998 en toch blijft fotografie mijn passie, maar heel gedreven in wat ik doe.
De kennis en ervaring doorgeven is mijn doel, voor de rest ben ik een klein beetje prettig gestoord.
Fotografie is een passie, een levenswijze. Als gepassioneerd landschap- en natuurfotograaf kan je mij steeds terug vinden in de natuur. Reeds van in mijn kinderjaren vertoef ik in de natuur. 
De  Bourgoyen in Gent waren dan ook mijn tweede thuis, deze lag op enkele meters van mijn ouderlijke woning.
Dezelfde passie en visie probeer ik nu door te geven via mijn workshops. 
Tevens in het volwassenenonderwijs kan je me terugvinden waar ik ook mijn kennis en ervaring door geef via de lesavonden.  
Landschap en Natuur is mijn voornaamste bezigheid, natuurlijk beelden moeten ook worden afgewerkt daarom komt mijn ervaring in beeld bewerking ook goed van pas. Hiervoor zijn ook de nodige workshops of lessen voorzien. 
";
        $event->save();

        $event = new Event;
        $event->event_name="Workshop";
        $event->title="macro fotografie";
        $event->name="Steve de Waele";
        $event->location= "studio-gebied1";
        $event->startdate="2018, 6, 16, 14, 30, 00";
        $event->enddate="2018, 6, 16, 16, 30, 00";
        $event->price="10";
        $event->quantity="14";
        $event->content="Welke materialen wordt er gebruikt bij macrofotografie.
Hoe ga je te werk
Licht beheersen en flitslicht controleren.

Voor deze workshops kan je best het volgende materiaal meebrengen ( niet verplicht alles te hebben)
Camerastatief 
Flits + swivel (indien je hierover beschikt)
Camera 
Macro lens
Witte paraplu of zwarte paraplu
Tussenringen 
Afstandsbediening 
70-200 ( ideaal macro objectief om van op afstand te werken)
Zender + ontvanger (indien je hierover beschikt)
";
        $event->bio="Wonende in Gent, gehuwd met Cindy en samen hebben wij drie kinderen. 
Zelfstandig sinds 1998 en toch blijft fotografie mijn passie, maar heel gedreven in wat ik doe.
De kennis en ervaring doorgeven is mijn doel, voor de rest ben ik een klein beetje prettig gestoord.
Fotografie is een passie, een levenswijze. Als gepassioneerd landschap- en natuurfotograaf kan je mij steeds terug vinden in de natuur. Reeds van in mijn kinderjaren vertoef ik in de natuur. 
De  Bourgoyen in Gent waren dan ook mijn tweede thuis, deze lag op enkele meters van mijn ouderlijke woning.
Dezelfde passie en visie probeer ik nu door te geven via mijn workshops. 
Tevens in het volwassenenonderwijs kan je me terugvinden waar ik ook mijn kennis en ervaring door geef via de lesavonden.  
Landschap en Natuur is mijn voornaamste bezigheid, natuurlijk beelden moeten ook worden afgewerkt daarom komt mijn ervaring in beeld bewerking ook goed van pas. Hiervoor zijn ook de nodige workshops of lessen voorzien. 
";
        $event->save();

        $event = new Event;
        $event->event_name="Workshop";
        $event->title="product fotografie";
        $event->name="Richard Buijsman";
        $event->location= "studio-gebied2";
        $event->startdate="2018, 6, 16, 10, 30, 00";
        $event->enddate="2018, 6, 16, 13, 00, 00";
        $event->price="10";
        $event->quantity="15";
        $event->content="In de huidige beeldcultuur is er grote vraag naar productfotografie. De vereisten zijn echter hoog: Opdrachtgevers willen correcte en flatterende beelden. Om dit te kunnen leveren, moet de techniek in de vingers zitten en dient creativiteit vooral voor het zetten van licht. 
Tijdens deze workshop gaan we samen stapsgewijs door het productieproces; gaande van de ontvangst van de opdracht, tot bestudering van het onderwerp, compositie, het zetten van licht en maken van de finale opnames. Er zal specifieke aandacht zijn voor perspectief, contrast-controle en het leren omgaan met glanzende en transparantie objecten. 
Deze workshop gaat vooral interactief zijn, waarin we samen al problem-solvend door de uitdagingen van productfotografie gaan. Neem gerust zelf objecten mee, zodat we de opname daarvan bespreken en doen, voor zover er tijd beschikbaar is.
";
        $event->bio="Richard van Boriann  is van opleiding geograaf en daarnaast is hij gediplomeerd en prijswinnend fotograaf. Hij is van iets noordelijkere origine en al 20 jaar professioneel actief in Vlaanderen. Momenteel runt hij fotostudio Boriann in Borsbeek, nabij Antwerpen, en specialiseert zich in product-, portret- en eventfotografie.";
        $event->save();

        $event = new Event;
        $event->event_name="Workshop";
        $event->title="product fotografie";
        $event->name="Richard Buijsman";
        $event->location= "studio-gebied2";
        $event->startdate="2018, 6, 16, 14, 00, 00";
        $event->enddate="2018, 6, 16, 16, 30, 00";
        $event->price="10";
        $event->quantity="15";
        $event->content="In de huidige beeldcultuur is er grote vraag naar productfotografie. De vereisten zijn echter hoog: Opdrachtgevers willen correcte en flatterende beelden. Om dit te kunnen leveren, moet de techniek in de vingers zitten en dient creativiteit vooral voor het zetten van licht. 
Tijdens deze workshop gaan we samen stapsgewijs door het productieproces; gaande van de ontvangst van de opdracht, tot bestudering van het onderwerp, compositie, het zetten van licht en maken van de finale opnames. Er zal specifieke aandacht zijn voor perspectief, contrast-controle en het leren omgaan met glanzende en transparantie objecten. 
Deze workshop gaat vooral interactief zijn, waarin we samen al problem-solvend door de uitdagingen van productfotografie gaan. Neem gerust zelf objecten mee, zodat we de opname daarvan bespreken en doen, voor zover er tijd beschikbaar is.
";
        $event->bio="Richard van Boriann  is van opleiding geograaf en daarnaast is hij gediplomeerd en prijswinnend fotograaf. Hij is van iets noordelijkere origine en al 20 jaar professioneel actief in Vlaanderen. Momenteel runt hij fotostudio Boriann in Borsbeek, nabij Antwerpen, en specialiseert zich in product-, portret- en eventfotografie.";
        $event->save();

        $event = new Event;
        $event->event_name="Workshop";
        $event->title="X Series";
        $event->name="Ioannis Tsouloulis";
        $event->location= "stand Fujifilm op de beurs";
        $event->startdate="2018, 6, 16, 10, 00, 00";
        $event->enddate="2018, 6, 16, 12, 30, 00";
        $event->price="0";
        $event->quantity="15";
        $event->content="Algemeen
Geschiedenis van de Fujifilm X-reeks
Technische verschillen tussen Fujifilm en andere fabrikanten
Voordelen van het Fujifilm gamma

Overzicht X Series
Voorstelling van het volledige X Series en GFX Series gamma
Inclusief de beschikbare lenzen
En focus op specifieke toepassingsgebieden

Portretsessie
Fashionshoot met flitslicht, 
Foto’s kunnen op een intax SHARE afgedrukt als aandenken 

Touch & feel experience
De cursisten kunnen zelf aan de slag onder begeleiding van de X Ambassador
Tegelijk is er ook mogelijkheid voor Q&A.
";
        $event->bio="Als professioneel fotograaf en Fujifilm X-ambassador heeft Ioannis Tsouloulis lange ervaring in fotografie en deelt hij graag met jou alle nuttige tips over juiste belichting om bv. knappe portretten te maken.
";
        $event->save();

        $event = new Event;
        $event->event_name="Workshop";
        $event->title="X Series";
        $event->name="Ioannis Tsouloulis";
        $event->location= "stand Fujifilm op de beurs";
        $event->startdate="2018, 6, 16, 14, 00, 00";
        $event->enddate="2018, 6, 16, 15, 30, 00";
        $event->price="0";
        $event->quantity="15";
        $event->content="Algemeen
Geschiedenis van de Fujifilm X-reeks
Technische verschillen tussen Fujifilm en andere fabrikanten
Voordelen van het Fujifilm gamma

Overzicht X Series
Voorstelling van het volledige X Series en GFX Series gamma
Inclusief de beschikbare lenzen
En focus op specifieke toepassingsgebieden

Portretsessie
Fashionshoot met flitslicht, 
Foto’s kunnen op een intax SHARE afgedrukt als aandenken 

Touch & feel experience
De cursisten kunnen zelf aan de slag onder begeleiding van de X Ambassador
Tegelijk is er ook mogelijkheid voor Q&A.
";
        $event->bio="Als professioneel fotograaf en Fujifilm X-ambassador heeft Ioannis Tsouloulis lange ervaring in fotografie en deelt hij graag met jou alle nuttige tips over juiste belichting om bv. knappe portretten te maken.
";
        $event->save();

        $event = new Event;
        $event->event_name="Demo";
        $event->title="Drone fotografie";
        $event->name="Carlo Meuws";
        $event->location= "nog te bepalen";
        $event->startdate="2018, 6, 16, 10, 00, 00";
        $event->enddate="2018, 6, 16, 12, 00, 00";
        $event->price="5";
        $event->quantity="20";
        $event->content="Gedurende deze 2 uur durende workshop worden de cursisten ondergedompeld in de wereld van drone-fotografie.
In een eerste theoretisch luik gaan we dieper in op de wetgeving, de drones en de camera’s. 
Tijdens het praktische gedeelte gaan we naar buiten waar we gaan vliegen en de theorie kunnen toetsen aan de praktijk.
";
        $event->bio="Sinds 2008 reist Carlo Meuws de wereld rond om luchtopnames te maken in opdracht van vooraanstaande productiehuizen en TV stations (Dakar, Red Bull Media house, Tomorrowland, VRT,…). In de beginperiode was dit vooral vanuit helikopters, maar sinds kort meer met drones. Graag neemt hij jullie mee achter de schermen van de hedendaagse luchtfotografie.
";
        $event->save();

        $event = new Event;
        $event->event_name="Demo";
        $event->title="Drone fotografie";
        $event->name="Carlo Meuws";
        $event->location= "nog te bepalen";
        $event->startdate="2018, 6, 16, 13, 00, 00";
        $event->enddate="2018, 6, 16, 15, 00, 00";
        $event->price="5";
        $event->quantity="20";
        $event->content="Gedurende deze 2 uur durende workshop worden de cursisten ondergedompeld in de wereld van drone-fotografie.
In een eerste theoretisch luik gaan we dieper in op de wetgeving, de drones en de camera’s. 
Tijdens het praktische gedeelte gaan we naar buiten waar we gaan vliegen en de theorie kunnen toetsen aan de praktijk.
";
        $event->bio="Sinds 2008 reist Carlo Meuws de wereld rond om luchtopnames te maken in opdracht van vooraanstaande productiehuizen en TV stations (Dakar, Red Bull Media house, Tomorrowland, VRT,…). In de beginperiode was dit vooral vanuit helikopters, maar sinds kort meer met drones. Graag neemt hij jullie mee achter de schermen van de hedendaagse luchtfotografie.
";
        $event->save();

        $event = new Event;
        $event->event_name="Demo";
        $event->title="Drone fotografie";
        $event->name="Carlo Meuws";
        $event->location= "nog te bepalen";
        $event->startdate="2018, 6, 16, 15, 00, 00";
        $event->enddate="2018, 6, 16, 17, 00, 00";
        $event->price="5";
        $event->quantity="20";
        $event->content="Gedurende deze 2 uur durende workshop worden de cursisten ondergedompeld in de wereld van drone-fotografie.
In een eerste theoretisch luik gaan we dieper in op de wetgeving, de drones en de camera’s. 
Tijdens het praktische gedeelte gaan we naar buiten waar we gaan vliegen en de theorie kunnen toetsen aan de praktijk.
";
        $event->bio="Sinds 2008 reist Carlo Meuws de wereld rond om luchtopnames te maken in opdracht van vooraanstaande productiehuizen en TV stations (Dakar, Red Bull Media house, Tomorrowland, VRT,…). In de beginperiode was dit vooral vanuit helikopters, maar sinds kort meer met drones. Graag neemt hij jullie mee achter de schermen van de hedendaagse luchtfotografie.
";
        $event->save();

        $event = new Event;
        $event->event_name="Workshop";
        $event->title="Continulicht/lichtflits";
        $event->name="Koen De Clerck";
        $event->location= "Lokaal 1.07";
        $event->startdate="2018, 6, 16, 10, 00, 00";
        $event->enddate="2018, 6, 16, 12, 00, 00";
        $event->price="10";
        $event->quantity="35";
        $event->content="Tijdens de demo-workshop wordt continu licht gecombineerd met flitslicht om de beweging van de danser zichtbaar te maken op een stilstaand beeld.  Theoretische uitleg over hoe we dat technisch aanpakken, welk materiaal het beste geschikt is voor dit soort opnamen en waarop je moet letten bij aankoop van flitsmateriaal voor dit doeleinde zal als introductie worden uitgelegd.  Dan zal een aantal dansbewegingen met deze techniek “in camera” worden gemaakt.  Er zal geen enkele beeldmanipulatie buiten de camera in post production nodig zijn.  Aan de hand van verschillende bewegingen zal de moeilijkheid van de timing bij het maken van dit soort foto’s worden getoond. Ook een vorm van light painting met gekleurde LED’s zal aan bod komen. 
";
        $event->bio="Koen de Cleck is woont in BelgIë en is een 53-jarige part-time industriële automatiseringsingenieur en heeft een tweede baan als fotograaf.
 
Hij begon  met fotograferen op 13-jarige leeftijd en reeds op die jonge leeftijd begon hij te werken als lichtontwerper en operator voor een semi professioneel modern dansgezelschap \"Terpsichore\". De samenwerking met verschillende dansgezelschappen, dansers en gymnasten duurde 20 jaar. 
Toen de bedrijven stopten met optreden, kon hij de ervaring die hij had opgebouwd met het fotograferen van dans gebruiken voor het maken van zijn eigen zwart-wit kunstfoto's. Dit evolueerde de afgelopen 20 jaar tot zwart-witte naakten met een theatrale sfeer met licht zoals hij dat deed tijdens de vele dansuitvoeringen die hij maakte.
";
        $event->save();

        $event = new Event;
        $event->event_name="Workshop";
        $event->title="Continulicht/lichtflits";
        $event->name="Koen De Clerck";
        $event->location= "Lokaal 1.07";
        $event->startdate="2018, 6, 16, 13, 30, 00";
        $event->enddate="2018, 6, 16, 15, 30, 00";
        $event->price="10";
        $event->quantity="35";
        $event->content="Tijdens de demo-workshop wordt continu licht gecombineerd met flitslicht om de beweging van de danser zichtbaar te maken op een stilstaand beeld.  Theoretische uitleg over hoe we dat technisch aanpakken, welk materiaal het beste geschikt is voor dit soort opnamen en waarop je moet letten bij aankoop van flitsmateriaal voor dit doeleinde zal als introductie worden uitgelegd.  Dan zal een aantal dansbewegingen met deze techniek “in camera” worden gemaakt.  Er zal geen enkele beeldmanipulatie buiten de camera in post production nodig zijn.  Aan de hand van verschillende bewegingen zal de moeilijkheid van de timing bij het maken van dit soort foto’s worden getoond. Ook een vorm van light painting met gekleurde LED’s zal aan bod komen. 
";
        $event->bio="Koen de Cleck is woont in BelgIë en is een 53-jarige part-time industriële automatiseringsingenieur en heeft een tweede baan als fotograaf.
 
Hij begon  met fotograferen op 13-jarige leeftijd en reeds op die jonge leeftijd begon hij te werken als lichtontwerper en operator voor een semi professioneel modern dansgezelschap \"Terpsichore\". De samenwerking met verschillende dansgezelschappen, dansers en gymnasten duurde 20 jaar. 
Toen de bedrijven stopten met optreden, kon hij de ervaring die hij had opgebouwd met het fotograferen van dans gebruiken voor het maken van zijn eigen zwart-wit kunstfoto's. Dit evolueerde de afgelopen 20 jaar tot zwart-witte naakten met een theatrale sfeer met licht zoals hij dat deed tijdens de vele dansuitvoeringen die hij maakte.
";
        $event->save();

        $event = new Event;
        $event->event_name="Wandeling";
        $event->title="Visiewandeling";
        $event->name="Jeffrey Vandaele";
        $event->location= "Cvo De Verdieping";
        $event->startdate="2018, 6, 16, 13, 00, 00";
        $event->enddate="2018, 6, 16, 14, 00, 00";
        $event->price="10";
        $event->quantity="15";
        $event->content="Tijdens een Visiewandeling leer je kritisch kijken naar de natuur met oog op beeldvorming. Je leert lichtsituaties inschatten, wanneer je welke composities kan gebruiken, hoe sfeer toe te voegen etc. Het is niet de bedoeling dat we fotograferen, het belangrijkste is dat we leren kijken en onze scene leren analyseren en ondervinden wat de beste compositie, belichting en techniek is om die scène zo optimaal mogelijk in beeld te brengen. Je gaat dus niet met mooie plaatjes naar huis maar wel met een enorme kennis rond beeldvorming. Het spreekt voor zich dat de tips die gegeven worden, afhankelijk zijn van de locatie en de licht- & weersomstandigheden.
";
        $event->bio="Jeffrey Van Daele (°1975) is een natuurfotograaf uit Sint-Lievens-Houtem met een passie en grote waardering voor alle natuurlijke schoonheid die ons omringt. Sinds 2010 is hij professioneel met fotografie bezig en kom je zijn werken in talloze publicaties tegen. Hij heeft een voorliefde voor macrofotografie en wildlife fotografie, maar ook zijn landschappen en abstracties worden door het grote publiek gesmaakt. Het materiaal dat hij daarvoor gebruikt, bestaat vooral uit Nikon camera’s en lenzen die een bereik hebben van 14 tot 500mm.

Naast het organiseren van workshops, lezingen en fotoreizen is hij al jaren een toegewijd leraar die zijn studenten inspireert en motiveert tijdens de lessen natuurfotografie en Lightroom voor natuurfotografen aan het volwassenenonderwijs.
";
        $event->save();

        $event = new Event;
        $event->event_name="Wandeling";
        $event->title="Visiewandeling";
        $event->name="Jeffrey Vandaele";
        $event->location= "Cvo De Verdieping";
        $event->startdate="2018, 6, 16, 14, 15, 00";
        $event->enddate="2018, 6, 16, 15, 15, 00";
        $event->price="10";
        $event->quantity="15";
        $event->content="Tijdens een Visiewandeling leer je kritisch kijken naar de natuur met oog op beeldvorming. Je leert lichtsituaties inschatten, wanneer je welke composities kan gebruiken, hoe sfeer toe te voegen etc. Het is niet de bedoeling dat we fotograferen, het belangrijkste is dat we leren kijken en onze scene leren analyseren en ondervinden wat de beste compositie, belichting en techniek is om die scène zo optimaal mogelijk in beeld te brengen. Je gaat dus niet met mooie plaatjes naar huis maar wel met een enorme kennis rond beeldvorming. Het spreekt voor zich dat de tips die gegeven worden, afhankelijk zijn van de locatie en de licht- & weersomstandigheden.
";
        $event->bio="Jeffrey Van Daele (°1975) is een natuurfotograaf uit Sint-Lievens-Houtem met een passie en grote waardering voor alle natuurlijke schoonheid die ons omringt. Sinds 2010 is hij professioneel met fotografie bezig en kom je zijn werken in talloze publicaties tegen. Hij heeft een voorliefde voor macrofotografie en wildlife fotografie, maar ook zijn landschappen en abstracties worden door het grote publiek gesmaakt. Het materiaal dat hij daarvoor gebruikt, bestaat vooral uit Nikon camera’s en lenzen die een bereik hebben van 14 tot 500mm.

Naast het organiseren van workshops, lezingen en fotoreizen is hij al jaren een toegewijd leraar die zijn studenten inspireert en motiveert tijdens de lessen natuurfotografie en Lightroom voor natuurfotografen aan het volwassenenonderwijs.
";
        $event->save();

        $event = new Event;
        $event->event_name="Wandeling";
        $event->title="Visiewandeling";
        $event->name="Jeffrey Vandaele";
        $event->location= "Cvo De Verdieping";
        $event->startdate="2018, 6, 16, 15, 30, 00";
        $event->enddate="2018, 6, 16, 16, 30, 00";
        $event->price="10";
        $event->quantity="15";
        $event->content="Tijdens een Visiewandeling leer je kritisch kijken naar de natuur met oog op beeldvorming. Je leert lichtsituaties inschatten, wanneer je welke composities kan gebruiken, hoe sfeer toe te voegen etc. Het is niet de bedoeling dat we fotograferen, het belangrijkste is dat we leren kijken en onze scene leren analyseren en ondervinden wat de beste compositie, belichting en techniek is om die scène zo optimaal mogelijk in beeld te brengen. Je gaat dus niet met mooie plaatjes naar huis maar wel met een enorme kennis rond beeldvorming. Het spreekt voor zich dat de tips die gegeven worden, afhankelijk zijn van de locatie en de licht- & weersomstandigheden.
";
        $event->bio="Jeffrey Van Daele (°1975) is een natuurfotograaf uit Sint-Lievens-Houtem met een passie en grote waardering voor alle natuurlijke schoonheid die ons omringt. Sinds 2010 is hij professioneel met fotografie bezig en kom je zijn werken in talloze publicaties tegen. Hij heeft een voorliefde voor macrofotografie en wildlife fotografie, maar ook zijn landschappen en abstracties worden door het grote publiek gesmaakt. Het materiaal dat hij daarvoor gebruikt, bestaat vooral uit Nikon camera’s en lenzen die een bereik hebben van 14 tot 500mm.

Naast het organiseren van workshops, lezingen en fotoreizen is hij al jaren een toegewijd leraar die zijn studenten inspireert en motiveert tijdens de lessen natuurfotografie en Lightroom voor natuurfotografen aan het volwassenenonderwijs.
";
        $event->save();

        $event = new Event;
        $event->event_name="Wandeling";
        $event->title="Visiewandeling";
        $event->name="Jeffrey Vandaele";
        $event->location= "Cvo De Verdieping";
        $event->startdate="2018, 6, 16, 16, 45, 00";
        $event->enddate="2018, 6, 16, 17, 45, 00";
        $event->price="10";
        $event->quantity="15";
        $event->content="Tijdens een Visiewandeling leer je kritisch kijken naar de natuur met oog op beeldvorming. Je leert lichtsituaties inschatten, wanneer je welke composities kan gebruiken, hoe sfeer toe te voegen etc. Het is niet de bedoeling dat we fotograferen, het belangrijkste is dat we leren kijken en onze scene leren analyseren en ondervinden wat de beste compositie, belichting en techniek is om die scène zo optimaal mogelijk in beeld te brengen. Je gaat dus niet met mooie plaatjes naar huis maar wel met een enorme kennis rond beeldvorming. Het spreekt voor zich dat de tips die gegeven worden, afhankelijk zijn van de locatie en de licht- & weersomstandigheden.
";
        $event->bio="Jeffrey Van Daele (°1975) is een natuurfotograaf uit Sint-Lievens-Houtem met een passie en grote waardering voor alle natuurlijke schoonheid die ons omringt. Sinds 2010 is hij professioneel met fotografie bezig en kom je zijn werken in talloze publicaties tegen. Hij heeft een voorliefde voor macrofotografie en wildlife fotografie, maar ook zijn landschappen en abstracties worden door het grote publiek gesmaakt. Het materiaal dat hij daarvoor gebruikt, bestaat vooral uit Nikon camera’s en lenzen die een bereik hebben van 14 tot 500mm.

Naast het organiseren van workshops, lezingen en fotoreizen is hij al jaren een toegewijd leraar die zijn studenten inspireert en motiveert tijdens de lessen natuurfotografie en Lightroom voor natuurfotografen aan het volwassenenonderwijs.
";
        $event->save();

        $event = new Event;
        $event->event_name="Lezing";
        $event->title="Door de lens van ...";
        $event->name="Ingrid Vekemans";
        $event->location= "ZLDR-terras atrium";
        $event->startdate="2018, 6, 16, 11, 30, 00";
        $event->enddate="2018, 6, 16, 12, 30, 00";
        $event->price="0";
        $event->quantity="-";
        $event->content="In thuisbasis België zijn wildlife, het bos en de macrowereld haar favoriete onderwerpen, maar ook landschapsfotografie draagt haar interesse weg. 
";
        $event->bio="Fotografe Ingrid Vekemans heeft een fascinatie voor Afrika en zette er voor het eerst voet aan de grond in 1994. Hoewel Afrika en zijn wildlife haar grootste passie zijn, ontwikkelde zij zich ook als veelzijdig natuurfotograaf dichter bij huis. 
        Ingrids werk werd meermaals internationaal bekroond en zij schrijft ook artikels voor vaktijdschriften zoals shoot, profifoto, zoom.nl, ....
";
        $event->save();

        $event = new Event;
        $event->event_name="Lezing";
        $event->title="Door de lens van ...";
        $event->name="Layla Aerts";
        $event->location= "ZLDR-terras atrium";
        $event->startdate="2018, 6, 16, 14, 15, 00";
        $event->enddate="2018, 6, 16, 15, 45, 00";
        $event->price="0";
        $event->quantity="-";
        $event->content="Gefascineerd door de relatie tussen realiteit en representatie overschrijdt haar werk de grens tussen journalistiek en kunst. Zowel de keuze van haar onderwerp als de manier van fotograferen zorgt ervoor dat we aan onze vooroordelen gaan twijfelen. Ze werk voornamelijk voor ngo’s, magazines, musea en overheden.

“Fotografie kan de toeschouwers in contact brengen met problemen en culturen die zich buiten hun leefwereld bevinden. Niet door te choqueren, medelijden te wekken of door sensatie maar door empathie, fascinatie en bewondering.”
";
        $event->bio="Layla Aerts studeerde af aan de Sint-Lucas Academie te Brussel met een thesis over ethiek en esthetiek in de documentaire fotografie.
Voor haar is een goede foto een beeld waarin ethiek en esthetiek elkaar in evenwicht houden en versterken. Haar fotoreportages zetten deze theorie om in de praktijk.\"Ze werkt nu meer dan 15 jaar als freelance fotografe voor ngo’s, kranten, magazine’s, musea,... Haar grootste interesse gaat uit naar kansarmoede en migratie. Ze heeft het geluk dat het grootste deel van mijn opdrachten over deze thema’s gaat. Ze vindt het belangrijk om ook vrij werk te maken. Reportages waar ze langer aan kan werken. Ze doet waar ze altijd van gedroomd heeft en hoopt ergens een klein verschil te kunnen maken.\"

";
        $event->save();
    }

}
